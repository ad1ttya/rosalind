//Counting Point Mutations file: rosalind_hamm.txt

open System.IO

let input = File.ReadAllLines "data/rosalind_hamm.txt"

let result =
    Seq.zip input.[0] input.[1]
    |> Seq.map (fun (x1, x2) -> if x1 <> x2 then 1 else 0)
    |> Seq.sum